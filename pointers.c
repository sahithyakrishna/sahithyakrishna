#include<stdio.h>
void swap(int*x,int*y)
{ int t;
  t=*x;
  *x=*y;
  *y=t;
  }
  int main()
  { int x,y;
   printf("Enter the value of x and y:");
scanf("%d%d",&x,&y);
printf("\nInitial values are: x=%d,y=%d",x,y);
swap(&x,&y);
printf("\nAfter swapping, values are x=%d,y=%d",x,y);
return 0;
}